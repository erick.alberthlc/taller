public class abstract SayayinDTO {
  private String nombre;
  private Integer nivelPoder;
  private static final Integer OVER_POWERED = 8000;
  
  ...
  
  public abstract void transformar();

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Integer getNivelPoder() {
    return nivelPoder;
  }

  public void setNivelPoder(Integer nivelPoder) {
    this.nivelPoder = nivelPoder;
  }

  public void transformar(){
	//AQUI HAY MUCHA LOGICA PROGRAMADA
  }


  public String generarMensajePoder(String name) {
    if (null != nivelPoder && OVER_POWERED.compareTo(nivelPoder) > 0) {
      return nombre + " It's over 9000";
    } else {
      return nombre + "Low power";
    }
  }

}
